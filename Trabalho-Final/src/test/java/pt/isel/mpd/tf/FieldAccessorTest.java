package pt.isel.mpd.tf;

import static org.junit.Assert.*;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.util.Map;

import org.junit.Test;

import pt.isel.mpd.tf.accessors.FieldAccessor;
import pt.isel.mpd.tf.accessors.FieldAccessorFactory;
import pt.isel.mpd.tf.accessors.MemberAccessor;
import pt.isel.mpd.tf.entities.Customer;

public class FieldAccessorTest {

	@Test
	public void testFieldAccessorGet() throws NoSuchFieldException,
			SecurityException, IllegalArgumentException, IllegalAccessException {
		// Arrange
		Customer c = new Customer("123", "Jo�o Carlos");
		Field f = Customer.class.getField("contactName");
		FieldAccessor fa = new FieldAccessor(f);

		// Act
		String name = (String) fa.get(c);

		// Assert
		assertNotNull(name);
		assertEquals(c.contactName, name);
	}

	@Test
	public void testFieldAccessorSet() throws NoSuchFieldException,
			SecurityException, IllegalArgumentException, IllegalAccessException {
		Customer c = new Customer("123", "Jo�o Carlos");
		Field f = Customer.class.getField("city");
		FieldAccessor fa = new FieldAccessor(f);
		String city = "Chelas";

		fa.set(c, city);

		assertNotNull(c.city);
		assertEquals(city, c.city);
	}

	@Test
	public void testFieldAccessorFactory() throws NoSuchFieldException,
			SecurityException, NoSuchMethodException {
		FieldAccessorFactory funBakery = new FieldAccessorFactory();
		Field[] fields = Customer.class.getFields();

		Map<String, MemberAccessor> m = funBakery
				.getMemberAccessors(Customer.class);

		assertEquals(11, m.size());
		for (int i = 0; i < fields.length; ++i) {
			Field expected = fields[i];

			MemberAccessor acc = m.get(expected.getName());
			AccessibleObject[] accObjs = acc.getAccessorObjects();

			assertNotNull(accObjs[0]);
			assertTrue(expected.equals(accObjs[0]));
		}
		assertNotNull(m.get("region"));
	}
}
