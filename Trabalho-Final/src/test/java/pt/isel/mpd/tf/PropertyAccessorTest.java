package pt.isel.mpd.tf;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import pt.isel.mpd.tf.accessors.MemberAccessor;
import pt.isel.mpd.tf.accessors.PropertyAccessor;
import pt.isel.mpd.tf.accessors.PropertyAccessorFactory;
import pt.isel.mpd.tf.entities.Customer;

public class PropertyAccessorTest {

	@Test
	public void testPropertyAccessorGet() throws NoSuchMethodException,
			SecurityException, IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		Customer c = new Customer("123", "Jo�o Carlos");
		Method getter = Customer.class
				.getMethod("getRegion", new Class<?>[] {});
		String region = "Hell";
		c.setRegion(region);
		PropertyAccessor pa = new PropertyAccessor(getter, null);

		String result = (String) pa.get(c);

		assertNotNull(result);
		assertEquals(region, result);
		assertEquals(c.region, result);
	}

	@Test
	public void testPropertyAccessorSet() throws NoSuchMethodException,
			SecurityException, IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		Customer c = new Customer("123", "Jo�o Carlos");
		Method setter = Customer.class.getMethod("setRegion",
				new Class<?>[] { String.class });
		String region = "Hell";
		PropertyAccessor pa = new PropertyAccessor(null, setter);

		pa.set(c, region);

		assertNotNull(c.region);
		assertEquals(region, c.region);
	}

	@Test
	public void testPropertyAccessorFactory() throws NoSuchMethodException,
			SecurityException {
		Map<String, MemberAccessor> expectedMap = new HashMap<>();
		Method setter;
		Method getter;
		for (Method m : Customer.class.getMethods()) {
			if (m.getName().contains("set")
					&& m.getParameterTypes().length == 1) {
				setter = m;
				getter = Customer.class.getMethod(
						setter.getName().replace("set", "get"),
						new Class<?>[] {});
				expectedMap.put(setter.getName().replace("set", ""),
						new PropertyAccessor(getter, setter));
			}
		}

		PropertyAccessorFactory factory = new PropertyAccessorFactory();
		Map<String, MemberAccessor> map = factory
				.getMemberAccessors(Customer.class);

		assertEquals(expectedMap.size(), map.size());
		for (Method method : Customer.class.getMethods()) {
			if (method.getName().contains("set")
					&& method.getParameterTypes().length == 1) {
				MemberAccessor ma = map
						.get(method.getName().replace("set", ""));
				assertNotNull(ma);
				assertTrue(ma.getAccessorObjects()[1].equals(method));
				assertTrue(ma.getAccessorObjects()[0].equals(Customer.class
						.getMethod(method.getName().replace("set", "get"),
								new Class<?>[] {})));
			}
		}
		assertNotNull(map.get("Region"));
	}
}
