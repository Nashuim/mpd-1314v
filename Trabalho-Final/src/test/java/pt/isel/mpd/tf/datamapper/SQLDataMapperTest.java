package pt.isel.mpd.tf.datamapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

import pt.isel.mpd.tf.Builder;
import pt.isel.mpd.tf.DataMapper;
import pt.isel.mpd.tf.SqlIterable;
import pt.isel.mpd.tf.accessors.FieldAccessorFactory;
import pt.isel.mpd.tf.accessors.MemberAccessorFactory;
import pt.isel.mpd.tf.builder.DataMapperBuilder;
import pt.isel.mpd.tf.builder.SqlDataMapperBuilder;
import pt.isel.mpd.tf.connection.SingletonConnectionManager;
import pt.isel.mpd.tf.entities.Employee;
import pt.isel.mpd.tf.entities.Order;
import pt.isel.mpd.tf.util.SqlConnectionBuilder;
import pt.isel.mpd.tf.util.SqlConnectionBuilder.Pair;
import pt.isel.mpd.tf.util.SqlStringBuilder;

public class SQLDataMapperTest {

	private SingletonConnectionManager conManager;
	private Builder builder;
	private MemberAccessorFactory fieldsFactory;
	private DataMapperBuilder sqlBuilder;

	@Before
	public void arrangeFortests() throws SQLException, FileNotFoundException,
			UnknownHostException, IOException {
		Pair<String, Properties> pair = SqlConnectionBuilder
				.buildSQLServerConnString();
		conManager = new SingletonConnectionManager(pair.first, pair.second);
		fieldsFactory = new FieldAccessorFactory();
		sqlBuilder = new SqlDataMapperBuilder(conManager);
	}

	@Test
	public void testSQLDataMapperDelete() throws NoSuchMethodException,
			SecurityException, SQLException {

		builder = new Builder(sqlBuilder, fieldsFactory);

		DataMapper<Order> sqlMapper = builder.build(Order.class);

		Connection con = conManager.beginBatch();

		String sql = SqlStringBuilder.beginInsert("Orders(ShipName)")
				.output("OrderID ").simpleValues(1).build();

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, "Matilda");

		ResultSet rs = ps.executeQuery();
		assertTrue(rs.next());

		Order matilda = new Order(rs.getInt(1));
		matilda.shipName = "Matilda";

		sqlMapper.delete(matilda);

		sql = SqlStringBuilder.beginSelect("Orders")
				.where("shipName = 'Matilda'").build();
		ps = con.prepareStatement(sql);
		rs = ps.executeQuery();

		assertFalse(rs.next());

		rs.close();
		conManager.rollback();
		conManager.endBatch();
	}

	@Test
	public void testSQLDataMapperUpdate() throws SQLException {
		builder = new Builder(sqlBuilder, fieldsFactory);
		DataMapper<Order> sqlMapper = builder.build(Order.class);
		Connection con = conManager.beginBatch();

		String sql = SqlStringBuilder
				.beginInsert("Orders(ShipName, shipAddress, shipCountry)")
				.output("OrderID ").simpleValues(3).build();

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, "this is not matilda");
		ps.setString(2, "this is not a tower");
		ps.setString(3, "England");

		ResultSet rs = ps.executeQuery();
		assertTrue(rs.next());

		Order matilda = new Order(rs.getInt(1));
		matilda.shipName = "Matilda";
		matilda.shipAddress = "Eiffel tower";
		matilda.shipCity = "Paris";
		matilda.shipCountry = "France";

		sqlMapper.update(matilda);

		sql = SqlStringBuilder.beginSelect("Orders")
				.where("ShipName = 'Matilda'").build();
		ps = con.prepareStatement(sql);
		rs = ps.executeQuery();

		assertTrue(rs.next());
		assertEquals(matilda.shipAddress, rs.getString("ShipAddress"));
		assertEquals(matilda.orderID.intValue(), rs.getInt(1));
		assertEquals(matilda.shipName, rs.getString("ShipName"));

		rs.close();
		conManager.rollback();
		conManager.endBatch();

	}

	@Test
	public void testSQLDataMapperInsert() throws SQLException {
		builder = new Builder(sqlBuilder, fieldsFactory);
		DataMapper<Order> sqlMapper = builder.build(Order.class);
		Connection con = conManager.beginBatch();

		Order zaros = new Order();
		zaros.shipName = "Mahjarrat";
		zaros.shipAddress = "that house over there in the wilderness";
		zaros.shipCity = "Ghorrock";
		zaros.shipCountry = "Gielinor";

		String sql = SqlStringBuilder.beginSelect("Orders")
				.where("ShipName = 'Mahjarrat'").build();

		PreparedStatement ps = con.prepareStatement(sql);

		sqlMapper.insert(zaros);

		ResultSet rs = ps.executeQuery();

		assertTrue(rs.next());
		assertEquals(zaros.shipName, rs.getString("ShipName"));
		assertEquals(zaros.shipAddress, rs.getString("ShipAddress"));
		assertEquals(zaros.shipCity, rs.getString("ShipCity"));
		assertEquals(zaros.shipCountry, rs.getString("ShipCountry"));

		rs.close();
		conManager.rollback();
		conManager.endBatch();
	}

	@Test
	public void testSQLDataMapperGetAll() throws SQLException {
		builder = new Builder(sqlBuilder, fieldsFactory);
		DataMapper<Order> sqlMapper = builder.build(Order.class);
		Connection con = conManager.beginBatch();

		String sql = SqlStringBuilder.beginSelect("Orders").build();

		PreparedStatement ps = con.prepareStatement(sql);

		Iterable<Order> newOrder = sqlMapper.getAll();

		ResultSet rs = ps.executeQuery();
		Iterator<Order> iter = newOrder.iterator();
		while (true) {
			boolean rsNext = rs.next();
			boolean iterNext = iter.hasNext();
			assertEquals(rsNext, iterNext);

			if (!rsNext)
				break;

			Order result = iter.next();
			assertEquals(result.orderID.intValue(), rs.getInt("OrderId"));
			assertEquals(result.shipName, rs.getString("ShipName"));
			assertEquals(result.shipCity, rs.getString("ShipCity"));
			assertEquals(result.shipCountry, rs.getString("ShipCountry"));
		}

		rs.close();
		conManager.rollback();
		conManager.endBatch();
	}

	@Test
	public void testSQLDataMapperGetAllWithAssociation() throws SQLException {
		builder = new Builder(sqlBuilder, fieldsFactory);
		DataMapper<Employee> sqlMapper = builder.build(Employee.class);
		Connection con = conManager.beginBatch();

		String employeeLevel1 = SqlStringBuilder.beginSelect("Employees")
				.where("ReportsTo = 5").build();

		PreparedStatement ps = con.prepareStatement(employeeLevel1);

		Iterable<Employee> employees = sqlMapper.getAll()
				.where("ReportsTo = 5");
		ResultSet rsLevel1 = ps.executeQuery();

		Iterator<Employee> firstEmployees = employees.iterator();

		String employeeLevel2 = ("select * from Employees where EmployeeId in (select ReportsTo from Employees where ReportsTo = '5')");
		ps = con.prepareStatement(employeeLevel2);
		ResultSet rsLevel2 = ps.executeQuery();
		rsLevel2.next();

		String employeeLevel3 = ("select * from Employees where EmployeeID in"
				+ "(select ReportsTo from Employees where EmployeeId in "
				+ "(select ReportsTo from Employees where ReportsTo = '5'))");
		ps = con.prepareStatement(employeeLevel3);
		ResultSet rsLevel3 = ps.executeQuery();
		rsLevel3.next();

		while (true) {
			if (!rsLevel1.next() || !firstEmployees.hasNext())
				break;

			Employee result = firstEmployees.next();
			assertEquals(rsLevel1.getInt("EmployeeID"), result.employeeID);
			assertEquals(rsLevel1.getString("FirstName"), result.firstName);
			assertEquals(rsLevel1.getString("LastName"), result.lastName);
			assertEquals(rsLevel1.getString("Address"), result.address);

			assertEquals(rsLevel2.getInt("EmployeeId"),
					result.reportsTo.employeeID);
			assertEquals(rsLevel2.getString("FirstName"),
					result.reportsTo.firstName);
			assertEquals(rsLevel2.getString("LastName"),
					result.reportsTo.lastName);
			assertEquals(rsLevel2.getString("Address"),
					result.reportsTo.address);

			assertEquals(rsLevel3.getInt("EmployeeId"),
					result.reportsTo.reportsTo.employeeID);
			assertEquals(rsLevel3.getString("FirstName"),
					result.reportsTo.reportsTo.firstName);
			assertEquals(rsLevel3.getString("LastName"),
					result.reportsTo.reportsTo.lastName);
			assertEquals(rsLevel3.getString("Address"),
					result.reportsTo.reportsTo.address);
			assertEquals(null, result.reportsTo.reportsTo.reportsTo);
		}
		assertEquals(rsLevel1.next(), firstEmployees.hasNext());

		rsLevel1.close();
		rsLevel2.close();
		rsLevel3.close();

		conManager.rollback();
		conManager.endBatch();
	}

	@Test
	public void testSQLDataMapperGetAllWithMultipleAssociation()
			throws SQLException {
		builder = new Builder(sqlBuilder, fieldsFactory);
		DataMapper<Employee> sqlMapper = builder.build(Employee.class);
		Connection con = conManager.beginBatch();

		String employee = SqlStringBuilder.beginSelect("Employees")
				.where("ReportsTo = 5").build();

		String ordersStr = "select * "
				+ "from Orders inner join Employees on Orders.EmployeeID = Employees.EmployeeID "
				+ "where Employees.ReportsTo = '5'";

		PreparedStatement ps = con.prepareStatement(employee);
		ResultSet rsEmployees = ps.executeQuery();

		ps = con.prepareStatement(ordersStr);
		ResultSet rsOrders = ps.executeQuery();

		SqlIterable<Employee> employeesIter = sqlMapper.getAll().where(
				"ReportsTo = 5");

		Iterator<Employee> employees = employeesIter.iterator();
		Iterator<Order> orders;

		while (true) {
			if (!rsEmployees.next() || employees.hasNext())
				break;
			Employee result = employees.next();
			assertEquals(rsEmployees.getInt("EmployeeID"), result.employeeID);
			assertEquals(rsEmployees.getString("FirstName"), result.firstName);
			assertEquals(rsEmployees.getString("LastName"), result.lastName);
			assertEquals(rsEmployees.getString("Address"), result.address);

			orders = result.orders.iterator();
			while (true) {
				if (!rsOrders.next() || orders.hasNext())
					break;
				Order resultOrder = orders.next();
				assertEquals(result.employeeID,
						resultOrder.employeeID.intValue());
				assertEquals(rsOrders.getInt("OrderID"),
						resultOrder.orderID.intValue());
				assertEquals(rsOrders.getString("ShipName"),
						resultOrder.shipName);
				assertEquals(rsOrders.getInt("EmployeeID"),
						resultOrder.employeeID.intValue());
				assertEquals(rsOrders.getString("ShipCity"),
						resultOrder.shipCity);
				assertEquals(rsOrders.getString("ShipCountry"),
						resultOrder.shipCountry);
			}
		}
		assertEquals(rsEmployees.next(), employees.hasNext());

		conManager.rollback();
		conManager.endBatch();
	}

	@Test
	public void testSQLDataMapperGetAllWithWhereClause() throws Exception {
		builder = new Builder(sqlBuilder, fieldsFactory);
		DataMapper<Order> sqlMapper = builder.build(Order.class);
		Connection con = conManager.beginBatch();

		String sql = SqlStringBuilder.beginSelect("Orders")
				.where("OrderID > 10267 and EmployeeID > 5").build();

		PreparedStatement ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();

		SqlIterable<Order> newOrder = sqlMapper.getAll()
				.where("OrderID > 10267 ").where("EmployeeID > 5");

		int size = sqlMapper.getAll().where("OrderID > 10267 ")
				.where("EmployeeID > 5").count();

		Iterator<Order> iter = newOrder.iterator();
		int count = 0;
		while (true) {
			if (!rs.next())
				break;

			Order result = iter.next();
			assertEquals(rs.getInt("OrderID"), result.orderID.intValue());
			assertEquals(rs.getString("ShipName"), result.shipName);
			assertEquals(rs.getInt("EmployeeID"), result.employeeID.intValue());
			assertEquals(rs.getString("ShipCity"), result.shipCity);
			assertEquals(rs.getString("ShipCountry"), result.shipCountry);
			++count;
		}
		assertEquals(rs.next(), iter.hasNext());
		assertEquals(count, size);

		rs.close();
		newOrder.close();
		conManager.rollback();
		conManager.endBatch();
	}

	@Test
	public void testSQLDataMapperBind() throws Exception {
		builder = new Builder(sqlBuilder, fieldsFactory);
		DataMapper<Order> sqlMapper = builder.build(Order.class);
		Connection con = conManager.beginBatch();

		String sql = SqlStringBuilder.beginSelect("Orders")
				.where("OrderID > 10267 and EmployeeID > 5").build();

		PreparedStatement ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();

		int orderId = 10267;
		int employeeId = 5;
		SqlIterable<Order> sqlIter = sqlMapper.getAll().where("OrderID > ? ")
				.where("EmployeeID > ?");
		SqlIterable<Order> newOrder = sqlIter.bind(orderId, employeeId);

		int size = sqlIter.bind(orderId, employeeId).count();

		Iterator<Order> iter = newOrder.iterator();
		int count = 0;
		while (true) {
			if (!rs.next())
				break;

			Order result = iter.next();
			assertEquals(rs.getInt("OrderID"), result.orderID.intValue());
			assertEquals(rs.getString("ShipName"), result.shipName);
			assertEquals(rs.getInt("EmployeeID"), result.employeeID.intValue());
			assertEquals(rs.getString("ShipCity"), result.shipCity);
			assertEquals(rs.getString("ShipCountry"), result.shipCountry);
			++count;
		}
		assertEquals(rs.next(), iter.hasNext());
		assertEquals(count, size);

		rs.close();
		sqlIter.close();
		newOrder.close();
		conManager.rollback();
		conManager.endBatch();
	}
}