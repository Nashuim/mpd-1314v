package pt.isel.mpd.tf.entities;

import java.sql.Timestamp;

import pt.isel.mpd.tf.annotation.ForeignKey;
import pt.isel.mpd.tf.annotation.PrimaryKey;
import pt.isel.mpd.tf.annotation.Table;

@Table("Employees")
public class Employee {

	@PrimaryKey
	public int employeeID;
	public String lastName;
	public String firstName;
	public String title;
	public String titleOfCourtesy;
	public Timestamp birthDate;
	public Timestamp hireDate;
	public String address;
	public String city;
	public String region;
	public String postalCode;
	public String country;
	public String homePhone;
	public String extension;
	public String notes;
	@ForeignKey("EmployeeID")
	public Employee reportsTo;
	public String photoPath;

	@ForeignKey("EmployeeID")
	public Iterable<Order> orders;
}
