package pt.isel.mpd.tf.connection;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.junit.Test;

import pt.isel.mpd.tf.util.SqlConnectionBuilder;
import pt.isel.mpd.tf.util.SqlConnectionBuilder.Pair;

public class BasicConnectionManagerTest {

	@Test
	public void connectionTest() throws SQLException, FileNotFoundException,
			UnknownHostException, IOException {
		Pair<String, Properties> pair = SqlConnectionBuilder
				.buildSQLServerConnString();
		BasicConnectionManager conManager = new BasicConnectionManager(
				pair.first, pair.second);
		Connection con = conManager.beginStatement();

		assertNotNull(con);
		assertTrue(con.isValid(0));
		conManager.endStatement(con);
		assertTrue(con.isClosed());
	}

	@Test
	public void multipleConnectionsTest() throws SQLException,
			FileNotFoundException, UnknownHostException, IOException {
		Pair<String, Properties> pair = SqlConnectionBuilder
				.buildSQLServerConnString();
		BasicConnectionManager conManager = new BasicConnectionManager(
				pair.first, pair.second);
		Connection con1 = conManager.beginStatement();
		Connection con2 = conManager.beginStatement();

		assertNotNull(con1);
		assertNotNull(con2);
		assertFalse(con1.equals(con2));

		assertTrue(con1.isValid(0));
		conManager.endStatement(con1);
		assertTrue(con1.isClosed());

		assertTrue(con2.isValid(0));
		conManager.endStatement(con2);
		assertTrue(con2.isClosed());
	}
}
