package pt.isel.mpd.tf.entities;

import java.math.BigDecimal;
import java.sql.Timestamp;

import pt.isel.mpd.tf.annotation.ForeignKey;
import pt.isel.mpd.tf.annotation.PrimaryKey;
import pt.isel.mpd.tf.annotation.Table;

@Table("Orders")
public class Order {

	@PrimaryKey(/* value = "OrderId" */)
	public Integer orderID;
	@ForeignKey("CustomerID")
	public Customer customerId;
	public Integer employeeID;
	public Timestamp orderDate;
	public Timestamp requiredDate;
	public Timestamp shippedDate;
	public Integer shipVia;
	public BigDecimal freight;
	public String shipName;
	public String shipAddress;
	public String shipCity;
	public String shipRegion;
	public String shipPostalCode;
	public String shipCountry;

	public Order(int orderId) {
		this.orderID = orderId;
	}

	public Order() {
	};

}
