package pt.isel.mpd.tf.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

public class SqlConnectionBuilder {

	private static String SQL_SERVER_STRING_BASE = "jdbc:sqlserver://";

	public static Properties getFromFile(String filename)
			throws FileNotFoundException, IOException {
		Properties prop = new Properties();

		prop.load(ClassLoader.getSystemResourceAsStream(filename));

		return prop;
	}

	public static Properties getFromMachine() throws FileNotFoundException,
			UnknownHostException, IOException {
		return getFromFile(InetAddress.getLocalHost().getHostName()
				+ ".properties");
	}

	public static Pair<String, Properties> buildSQLServerConnString()
			throws FileNotFoundException, UnknownHostException, IOException {
		Properties prop = getFromMachine();

		return new Pair<String, Properties>(SQL_SERVER_STRING_BASE, prop);

	}

	public static class Pair<T, U> {
		public T first;
		public U second;

		public Pair(T first, U second) {
			this.first = first;
			this.second = second;
		}
	}
}
