package pt.isel.mpd.tf.datamapper;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import pt.isel.mpd.tf.Builder;
import pt.isel.mpd.tf.DataMapper;
import pt.isel.mpd.tf.accessors.FieldAccessorFactory;
import pt.isel.mpd.tf.accessors.MemberAccessorFactory;
import pt.isel.mpd.tf.accessors.PropertyAccessorFactory;
import pt.isel.mpd.tf.builder.ListDataMapperBuilder;
import pt.isel.mpd.tf.entities.Customer;

public class ListDataMapperTest {

	private Customer c;
	private List<Customer> customers;

	private MemberAccessorFactory fieldFactory;
	private MemberAccessorFactory propertyFactory;

	private ListDataMapperBuilder dataMapperBuilder;
	private Builder builderWithFields;
	private Builder builderWithProperties;

	private DataMapper<Customer> dataMapperWithFields;
	private DataMapper<Customer> dataMapperWithProperties;

	private Iterable<Customer> col;

	@Before
	public void arrangeForTests() {
		fieldFactory = new FieldAccessorFactory();
		propertyFactory = new PropertyAccessorFactory();

		customers = new ArrayList<Customer>();
		c = new Customer("123", "Sam");

		dataMapperBuilder = new ListDataMapperBuilder(customers);
	}

	@Test
	public void testListDataMapperInsertWithFields() {
		// Arrange
		builderWithFields = new Builder(dataMapperBuilder, fieldFactory);
		dataMapperWithFields = builderWithFields.build(Customer.class);
		int number = 0;
		int expectedNumber = 1;

		// Act
		dataMapperWithFields.insert(c);
		col = dataMapperWithFields.getAll();

		// Assert
		assertNotNull(col);

		for (Customer cos : col) {
			assertEquals(c.customerId, cos.customerId);
			assertEquals(c.contactName, cos.contactName);
			++number;
		}
		assertEquals(expectedNumber, number);
	}

	@Test
	public void testListDataMapperInsertWithProperties() {
		// Arrange
		c.setRegion("Hell");
		builderWithProperties = new Builder(dataMapperBuilder, propertyFactory);
		dataMapperWithProperties = builderWithProperties.build(Customer.class);
		int number = 0;
		int expectedNumber = 1;

		// Act
		dataMapperWithProperties.insert(c);
		col = dataMapperWithProperties.getAll();

		// Assert
		assertNotNull(col);

		for (Customer cos : col) {
			assertEquals(c.getCostumerId(), cos.getCostumerId());
			assertEquals(c.getRegion(), cos.getRegion());
			++number;
		}
		assertEquals(expectedNumber, number);
	}

	@Test
	public void testListDataMapperUpdateWithFields() {
		// Arrange
		Customer newCos = new Customer("123", "John Snow");
		builderWithFields = new Builder(dataMapperBuilder, fieldFactory);
		dataMapperWithFields = builderWithFields.build(Customer.class);
		int number = 0;
		int expectedNumber = 1;

		// Act
		dataMapperWithFields.insert(c);
		dataMapperWithFields.update(newCos);
		col = dataMapperWithFields.getAll();

		// Assert
		assertNotNull(col);
		for (Customer cos : col) {
			assertEquals(newCos.customerId, cos.customerId);
			assertEquals(newCos.contactName, cos.contactName);
			++number;
		}
		assertEquals(expectedNumber, number);
	}

	@Test
	public void testListDataMapperUpdateWithProperties() {
		// Arrange
		c.setRegion("Hell");
		Customer newCos = new Customer("123", "Daenerys");
		newCos.setRegion("Westeros");
		builderWithProperties = new Builder(dataMapperBuilder, propertyFactory);
		dataMapperWithProperties = builderWithProperties.build(Customer.class);
		int number = 0;
		int expectedNumber = 1;

		// Act
		dataMapperWithProperties.insert(c);
		dataMapperWithProperties.update(newCos);
		col = dataMapperWithProperties.getAll();

		// Assert
		assertNotNull(col);

		for (Customer cos : col) {
			assertEquals(newCos.getCostumerId(), cos.getCostumerId());
			assertEquals(newCos.getRegion(), cos.getRegion());
			assertNotEquals(c.getRegion(), cos.getRegion());
			++number;
		}
		assertEquals(expectedNumber, number);
	}

	@Test
	public void testListDataMapperRemove() {
		// Arrange
		Customer newCos = new Customer("1337", "Joffrey");
		builderWithFields = new Builder(dataMapperBuilder, fieldFactory);
		dataMapperWithFields = builderWithFields.build(Customer.class);
		int number = 0;
		int expectedNumber = 1;

		// Act
		dataMapperWithFields.insert(c);
		dataMapperWithFields.insert(newCos);
		dataMapperWithFields.delete(c);
		col = dataMapperWithFields.getAll();

		// Assert
		assertNotNull(col);

		for (Customer cos : col) {
			assertNotEquals(c.customerId, cos.customerId);
			assertNotEquals(c.contactName, cos.contactName);
			++number;
		}
		assertEquals(expectedNumber, number);
	}
}
