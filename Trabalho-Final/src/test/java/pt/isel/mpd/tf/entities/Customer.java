package pt.isel.mpd.tf.entities;

import pt.isel.mpd.tf.annotation.PrimaryKey;
import pt.isel.mpd.tf.annotation.Table;

@Table("Customers")
public class Customer {

	@PrimaryKey(identity = false)
	public String customerId;
	public String companyName;
	public String contactName;
	public String contactTitle;
	public String address;
	public String city;
	public String region;
	public String postalCode;
	public String country;
	public String phone;
	public String fax;

	public Customer() {
	}

	public Customer(String id, String name) {
		this.customerId = id;
		this.contactName = name;
	}

	public void setCostumerId(String id) {
		this.customerId = id;
	}

	public String getCostumerId() {
		return this.customerId;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getRegion() {
		return this.region;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Customer))
			return false;
		Customer cos = (Customer) obj;
		return this.customerId == cos.getCostumerId();
	}
}
