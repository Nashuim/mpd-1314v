package pt.isel.mpd.tf.builder;

import java.util.List;
import java.util.Map;

import pt.isel.mpd.tf.DataMapper;
import pt.isel.mpd.tf.accessors.MemberAccessor;
import pt.isel.mpd.tf.datamapper.ListDataMapper;

public class ListDataMapperBuilder implements DataMapperBuilder {

	private List<?> col;

	public ListDataMapperBuilder(List<?> col) {
		this.col = col;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> DataMapper<T> build(Class<T> klass,
			Map<String, MemberAccessor> map) {
		return new ListDataMapper<T>(klass, map, (List<T>) col);
	}

}
