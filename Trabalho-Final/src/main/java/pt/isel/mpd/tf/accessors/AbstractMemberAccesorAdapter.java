package pt.isel.mpd.tf.accessors;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.Map;

import pt.isel.mpd.tf.DataMapper;
import pt.isel.mpd.tf.annotation.ForeignKey;

public abstract class AbstractMemberAccesorAdapter implements MemberAccessor {

	DataMapper<?> mapper;
	ForeignKey fk;
	MemberAccessor mac;

	public AbstractMemberAccesorAdapter(Class<?> fkClass,
			Map<String, MemberAccessor> m2, MemberAccessor mac, ForeignKey fk,
			DataMapper<?> build) {
		this.mapper = build;
		this.fk = fk;
		this.mac = mac;
	}

	@Override
	public Object get(Object obj) throws IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		return null;
	}

	@Override
	public void set(Object obj, Object value) throws IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		if (value == null)
			mac.set(obj, null);
		else
			internalSet(obj, value);

	}

	protected abstract void internalSet(Object obj, Object value)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException;

	@Override
	public AccessibleObject[] getAccessorObjects() {
		return new AccessibleObject[] {};
	}

	@Override
	public <T extends Annotation> T getAnnotation(Class<T> annClass) {
		return mac.getAnnotation(annClass);
	}

	@Override
	public Class<?> getType() {
		return mac.getType();
	}

	@Override
	public ParameterizedType getGenericType() {
		return mac.getGenericType();
	}

	@Override
	public String getName() {
		return fk.value();
	}

}
