package pt.isel.mpd.tf.accessors;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import pt.isel.mpd.tf.DataMapper;
import pt.isel.mpd.tf.annotation.ForeignKey;

public class MultipleMemberAccessor extends AbstractMemberAccesorAdapter {

	public MultipleMemberAccessor(Class<?> fkClass,
			Map<String, MemberAccessor> m2, MemberAccessor mac, ForeignKey fk,
			DataMapper<?> build) {
		super(fkClass, m2, mac, fk, build);
	}

	@Override
	protected void internalSet(Object obj, Object value)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException {

		mac.set(obj, mapper.getAll().where(fk.value() + " = '" + value + "'"));

	}

}
