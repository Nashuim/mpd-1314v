package pt.isel.mpd.tf.builder;

import java.util.Map;

import pt.isel.mpd.tf.DataMapper;
import pt.isel.mpd.tf.accessors.MemberAccessor;

public interface DataMapperBuilder {

	<T> DataMapper<T> build(Class<T> klass, Map<String, MemberAccessor> map);

}
