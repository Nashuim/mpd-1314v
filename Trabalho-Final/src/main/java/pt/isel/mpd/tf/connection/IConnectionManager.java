package pt.isel.mpd.tf.connection;

import java.sql.Connection;
import java.sql.SQLException;

public interface IConnectionManager {

	Connection beginStatement() throws SQLException;

	void endStatement(Connection con) throws SQLException;

}
