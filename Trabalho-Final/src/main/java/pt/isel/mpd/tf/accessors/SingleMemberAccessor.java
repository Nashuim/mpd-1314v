package pt.isel.mpd.tf.accessors;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import pt.isel.mpd.tf.DataMapper;
import pt.isel.mpd.tf.annotation.ForeignKey;

public class SingleMemberAccessor extends AbstractMemberAccesorAdapter {

	public SingleMemberAccessor(Class<?> fkClass,
			Map<String, MemberAccessor> m2, MemberAccessor mac, ForeignKey fk,
			DataMapper<?> build) {
		super(fkClass, m2, mac, fk, build);
	}

	@Override
	protected void internalSet(Object obj, Object value)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException {
		for (Object o : mapper.getAll()
				.where(fk.value() + " = '" + value + "'")) {
			mac.set(obj, o);
			return;
		}
	}

}
