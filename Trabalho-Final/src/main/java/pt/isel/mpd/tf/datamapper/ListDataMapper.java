package pt.isel.mpd.tf.datamapper;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import pt.isel.mpd.tf.SqlIterable;
import pt.isel.mpd.tf.accessors.MemberAccessor;
import pt.isel.mpd.tf.exception.BuilderException;

public class ListDataMapper<T> extends AbstractDataMapper<T> {

	private List<T> col;

	public ListDataMapper(Class<T> klass, Map<String, MemberAccessor> map,
			List<T> col) {
		super(klass, map);
		this.col = col;
	}

	public SqlIterable<T> getAll() {
		return new SqlIterable<T>() {

			@Override
			public Iterator<T> iterator() {
				return col.iterator();
			}

			@Override
			public void close() throws Exception {
				// TODO Auto-generated method stub

			}

			@Override
			public SqlIterable<T> where(String clause) {
				// We're not implementing where in the ListDataMapper because
				// life is too short
				throw new UnsupportedOperationException(
						"You shouldn't ask 'where', you should just go.");
			}

			@Override
			public int count() {
				return col.size();
			}

			@Override
			public SqlIterable<T> bind(Object... objs) {
				throw new UnsupportedOperationException(
						"Nothing can bind a free spirit.");

			}
		};
	}

	public void update(T val) {
		T obj = col.get(col.indexOf(val));
		replicateInto(obj, val);

	}

	public void delete(T val) {
		// Replicating the object for tests
		col.remove(replicate(val));

	}

	public void insert(T val) {
		// Replicating the object for tests
		col.add(replicate(val));

	}

	private T replicate(T val) {

		try {
			return replicateInto(getKlass().newInstance(), val);
		} catch (Exception e) {
			throw new BuilderException("Error creating instance", e);
		}

	}

	private T replicateInto(T obj, T val) {
		iterateAndExecute((Map.Entry<String, MemberAccessor> e) -> {
			MemberAccessor acc = e.getValue();
			try {
				Object valFrom = acc.get(val);
				acc.set(obj, valFrom);
			} catch (Exception ex) {
				throw new BuilderException("Error accessing object", ex);
			}
		});

		return obj;
	}
}
