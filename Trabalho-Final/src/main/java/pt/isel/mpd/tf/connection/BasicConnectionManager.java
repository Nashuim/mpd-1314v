package pt.isel.mpd.tf.connection;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class BasicConnectionManager extends AbstractConnectionManager {

	public BasicConnectionManager(String url) {
		super(url);
	}

	public BasicConnectionManager(String url, Properties prop) {
		super(url, prop);
	}

	@Override
	public Connection beginStatement() throws SQLException {
		return createConnection();
	}

	@Override
	public void endStatement(Connection con) throws SQLException {
		con.close();
	}

}
