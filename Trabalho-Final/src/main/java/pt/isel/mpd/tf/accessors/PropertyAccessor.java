package pt.isel.mpd.tf.accessors;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;

public class PropertyAccessor implements MemberAccessor {

	private Method getter;
	private Method setter;

	public PropertyAccessor(Method getter, Method setter) {
		this.getter = getter;
		this.setter = setter;
	}

	@Override
	public Object get(Object obj) throws IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		return getter.invoke(obj, new Object[] {});
	}

	@Override
	public void set(Object obj, Object value) throws IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		setter.invoke(obj, value);
	}

	@Override
	public AccessibleObject[] getAccessorObjects() {
		return new AccessibleObject[] { getter, setter };
	}

	@Override
	public String getName() {
		return getter.getName().substring(3);
	}

	@Override
	public <T extends Annotation> T getAnnotation(Class<T> annClass) {
		T ann = getter.getAnnotation(annClass);
		if (ann == null)
			return setter.getAnnotation(annClass);
		else
			return ann;
	}

	@Override
	public Class<?> getType() {
		return getter.getReturnType();
	}

	@Override
	public ParameterizedType getGenericType() {
		return (ParameterizedType) getter.getGenericReturnType();
	}
}