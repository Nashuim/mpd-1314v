package pt.isel.mpd.tf.accessors;

import java.lang.reflect.Field;
import java.util.Map;

public class FieldAccessorFactory extends AbstractMemberAccessorFactory {

	@Override
	protected void createNewMemberAccessors(Class<?> klass,
			Map<String, MemberAccessor> map) {

		for (Field field : klass.getFields()) {
			map.put(field.getName(), new FieldAccessor(field));
		}

	}
}
