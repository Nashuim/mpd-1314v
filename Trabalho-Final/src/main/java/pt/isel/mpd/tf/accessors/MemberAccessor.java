package pt.isel.mpd.tf.accessors;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;

public interface MemberAccessor {

	Object get(Object obj) throws IllegalArgumentException,
			IllegalAccessException, InvocationTargetException;

	void set(Object obj, Object value) throws IllegalArgumentException,
			IllegalAccessException, InvocationTargetException;

	AccessibleObject[] getAccessorObjects();

	<T extends Annotation> T getAnnotation(Class<T> annClass);

	Class<?> getType();

	ParameterizedType getGenericType();

	String getName();
}
