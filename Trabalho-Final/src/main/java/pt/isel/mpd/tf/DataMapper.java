package pt.isel.mpd.tf;

public interface DataMapper<T> {

	SqlIterable<T> getAll();

	void update(T val);

	void delete(T val);

	void insert(T val);

}
