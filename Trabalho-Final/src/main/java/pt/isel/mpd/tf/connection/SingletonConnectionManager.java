package pt.isel.mpd.tf.connection;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class SingletonConnectionManager extends AbstractConnectionManager
		implements ISingletonConnectionManager {

	private Connection con;

	public SingletonConnectionManager(String url) throws SQLException {
		super(url);
	}

	public SingletonConnectionManager(String url, Properties prop) {
		super(url, prop);
	}

	@Override
	public Connection beginStatement() throws SQLException {
		return con;
	}

	@Override
	public void endStatement(Connection con) throws SQLException {
	}

	@Override
	public Connection beginBatch() throws SQLException {
		this.con = createConnection();
		con.setAutoCommit(false);
		return con;
	}

	@Override
	public void endBatch() throws SQLException {
		con.commit();
		con.close();
	}

	public void rollback() throws SQLException {
		con.rollback();
	}

}
