package pt.isel.mpd.tf.accessors;

import java.lang.reflect.Method;
import java.util.Map;

public class PropertyAccessorFactory extends AbstractMemberAccessorFactory {

	@Override
	protected void createNewMemberAccessors(Class<?> klass,
			Map<String, MemberAccessor> map) throws NoSuchMethodException,
			SecurityException {
		Method setter;
		Method getter;
		for (Method m : klass.getMethods()) {
			if (m.getName().contains("set")
					&& m.getParameterTypes().length == 1) {
				setter = m;
				getter = klass.getMethod(
						setter.getName().replace("set", "get"),
						new Class<?>[] {});
				map.put(setter.getName().replace("set", ""),
						new PropertyAccessor(getter, setter));
			}
		}

	}

}
