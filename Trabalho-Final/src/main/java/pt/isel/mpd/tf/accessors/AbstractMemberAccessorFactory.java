package pt.isel.mpd.tf.accessors;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractMemberAccessorFactory implements
		MemberAccessorFactory {

	private Map<Class<?>, Map<String, MemberAccessor>> classMap = new HashMap<Class<?>, Map<String, MemberAccessor>>();

	protected abstract void createNewMemberAccessors(Class<?> klass,
			Map<String, MemberAccessor> map) throws NoSuchMethodException,
			SecurityException;

	@Override
	public Map<String, MemberAccessor> getMemberAccessors(Class<?> klass)
			throws NoSuchMethodException, SecurityException {
		if (classMap.containsKey(klass))
			return classMap.get(klass);

		Map<String, MemberAccessor> map = new HashMap<>();
		createNewMemberAccessors(klass, map);

		classMap.put(klass, map);
		return map;
	}

}
