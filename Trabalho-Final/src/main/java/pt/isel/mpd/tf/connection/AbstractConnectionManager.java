package pt.isel.mpd.tf.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public abstract class AbstractConnectionManager implements IConnectionManager {

	private String url;
	private Properties prop;

	public AbstractConnectionManager(String url) {
		this.url = url;
	}

	public AbstractConnectionManager(String url, Properties prop) {
		this.url = url;
		this.prop = prop;
	}

	protected Connection createConnection() throws SQLException {
		if (prop == null)
			return DriverManager.getConnection(url);
		else
			return DriverManager.getConnection(url, prop);
	}
}
