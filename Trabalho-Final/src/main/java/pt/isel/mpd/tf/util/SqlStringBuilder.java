package pt.isel.mpd.tf.util;

public class SqlStringBuilder {

	private static final String SELECT_BASE = "SELECT * FROM %s ";
	private static final String SELECT_COUNT_BASE = "SELECT COUNT(*) FROM %s ";
	private static final String DELETE_BASE = "DELETE FROM %s ";
	private static final String INSERT_BASE = "INSERT INTO %s ";
	private static final String UPDATE_BASE = "UPDATE %s ";
	private static final String WHERE_BASE = "WHERE ";
	private static final String VALUES = "VALUES ";
	private static final String SET = "SET ";
	private static final String OPENING = "( ";
	private static final String CLOSING = ") ";
	private static final String OUTPUT = "OUTPUT INSERTED.";
	private static final Object AND = "AND ";

	private StringBuilder builder = new StringBuilder();

	public static SqlStringBuilder beginDelete(String tableName) {
		return new DeleteSqlStringBuilder(tableName);
	}

	public static SqlStringBuilder beginInsert(String tableName) {
		return new InsertSqlStringBuilder(tableName);
	}

	public static SqlStringBuilder beginUpdate(String tableName) {
		return new UpdateSqlStringBuilder(tableName);
	}

	public static SqlStringBuilder beginSelect(String tableName) {
		return new SelectSqlStringBuilder(tableName);
	}

	public static SqlStringBuilder beginSelectCount(String tableName) {
		return new SelectCountSqlStringBuilder(tableName);
	}

	public static SqlStringBuilder continueFrom(String sql) {
		SqlStringBuilder sqlBuilder = new SqlStringBuilder();
		sqlBuilder.builder.append(sql);
		return sqlBuilder;
	}

	public SqlStringBuilder where(String where) {
		builder.append(WHERE_BASE + where);
		return this;
	}

	public SqlStringBuilder values(String[] values) {
		StringBuilder aux = new StringBuilder(VALUES).append(OPENING);
		builder.append(OPENING);
		for (int i = 0; i < values.length - 1; ++i) {
			builder.append(values[i] + ", ");
			aux.append("?, ");
		}
		aux.append("?").append(CLOSING);
		builder.append(values[values.length - 1]).append(CLOSING).append(aux);

		return this;
	}

	public SqlStringBuilder simpleValues(int i) {
		builder.append(VALUES + OPENING);
		if (i > 0) {
			builder.append("?");
			--i;
		}

		for (; i > 0; --i)
			builder.append(", ?");

		builder.append(CLOSING);

		return this;
	}

	public SqlStringBuilder set(String[] values) {
		builder.append(SET);

		for (int i = 0; i < values.length - 1; ++i) {
			builder.append(values[i] + " = ?, ");
		}
		builder.append(values[values.length - 1] + " = ? ");
		return this;
	}

	public SqlStringBuilder output(String value) {
		builder.append(OUTPUT);
		builder.append(value);
		return this;
	}

	public SqlStringBuilder and() {
		builder.append(AND);
		return this;
	}

	public SqlStringBuilder append(String clause) {
		builder.append(clause);
		return this;
	}

	public String build() {
		return builder.toString();
	}

	protected void setInitialString(String init) {
		builder.append(init);
	}

	private static class DeleteSqlStringBuilder extends SqlStringBuilder {
		public DeleteSqlStringBuilder(String tableName) {
			setInitialString(String.format(DELETE_BASE, tableName));
		}
	}

	private static class InsertSqlStringBuilder extends SqlStringBuilder {
		public InsertSqlStringBuilder(String tableName) {
			setInitialString(String.format(INSERT_BASE, tableName));
		}
	}

	private static class UpdateSqlStringBuilder extends SqlStringBuilder {

		public UpdateSqlStringBuilder(String tableName) {
			setInitialString(String.format(UPDATE_BASE, tableName));
		}

	}

	private static class SelectSqlStringBuilder extends SqlStringBuilder {

		public SelectSqlStringBuilder(String tableName) {
			setInitialString(String.format(SELECT_BASE, tableName));
		}
	}

	private static class SelectCountSqlStringBuilder extends SqlStringBuilder {

		public SelectCountSqlStringBuilder(String tableName) {
			setInitialString(String.format(SELECT_COUNT_BASE, tableName));
		}
	}
}
