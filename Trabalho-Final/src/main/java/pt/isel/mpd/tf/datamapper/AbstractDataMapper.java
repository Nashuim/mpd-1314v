package pt.isel.mpd.tf.datamapper;

import java.util.Map;
import java.util.function.Consumer;

import pt.isel.mpd.tf.DataMapper;
import pt.isel.mpd.tf.accessors.MemberAccessor;
import pt.isel.mpd.tf.exception.BuilderException;

public abstract class AbstractDataMapper<T> implements DataMapper<T> {

	private Map<String, MemberAccessor> accessors;
	private Class<T> klass;

	public AbstractDataMapper(Class<T> klass, Map<String, MemberAccessor> map) {
		this.setAccessors(map);
		this.setKlass(klass);
	}

	protected Map<String, MemberAccessor> getAccessors() {
		return accessors;
	}

	protected void setAccessors(Map<String, MemberAccessor> accessors) {
		this.accessors = accessors;
	}

	protected Class<T> getKlass() {
		return klass;
	}

	protected void setKlass(Class<T> klass) {
		this.klass = klass;
	}

	protected void iterateAndExecute(
			Consumer<Map.Entry<String, MemberAccessor>> cons) {
		for (Map.Entry<String, MemberAccessor> entry : accessors.entrySet()) {
			try {
				cons.accept(entry);
			} catch (Exception e) {
				throw new BuilderException("Error accessing object", e);
			}
		}
	}
}
