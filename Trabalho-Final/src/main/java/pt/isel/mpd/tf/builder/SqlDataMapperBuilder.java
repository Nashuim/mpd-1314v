package pt.isel.mpd.tf.builder;

import java.util.Map;

import pt.isel.mpd.tf.DataMapper;
import pt.isel.mpd.tf.accessors.MemberAccessor;
import pt.isel.mpd.tf.connection.IConnectionManager;
import pt.isel.mpd.tf.datamapper.SqlDataMapper;

public class SqlDataMapperBuilder implements DataMapperBuilder {

	private IConnectionManager manager;

	public SqlDataMapperBuilder(IConnectionManager manager) {
		this.manager = manager;
	}

	@Override
	public <T> DataMapper<T> build(Class<T> klass,
			Map<String, MemberAccessor> map) {
		return new SqlDataMapper<T>(klass, map, manager);
	}

}
