package pt.isel.mpd.tf.accessors;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;

public class FieldAccessor implements MemberAccessor {
	private Field field;

	public FieldAccessor(Field field) {
		this.field = field;
	}

	public Object get(Object obj) throws IllegalArgumentException,
			IllegalAccessException {
		return field.get(obj);
	}

	public void set(Object obj, Object value) throws IllegalArgumentException,
			IllegalAccessException {
		field.set(obj, value);
	}

	@Override
	public AccessibleObject[] getAccessorObjects() {
		return new AccessibleObject[] { field };
	}

	@Override
	public String getName() {
		return field.getName();
	}

	@Override
	public <T extends Annotation> T getAnnotation(Class<T> annClass) {
		return field.getAnnotation(annClass);
	}

	@Override
	public Class<?> getType() {
		return field.getType();
	}

	@Override
	public ParameterizedType getGenericType() {
		return (ParameterizedType) field.getGenericType();
	}
}
