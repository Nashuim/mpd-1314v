package pt.isel.mpd.tf;

public interface SqlIterable<T> extends Iterable<T>, AutoCloseable {
	SqlIterable<T> where(String clause);

	int count();

	SqlIterable<T> bind(Object... objs);
}
