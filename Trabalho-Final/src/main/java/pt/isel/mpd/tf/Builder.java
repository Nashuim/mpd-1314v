package pt.isel.mpd.tf;

import java.util.HashMap;
import java.util.Map;

import pt.isel.mpd.tf.accessors.MemberAccessor;
import pt.isel.mpd.tf.accessors.MemberAccessorFactory;
import pt.isel.mpd.tf.accessors.MultipleMemberAccessor;
import pt.isel.mpd.tf.accessors.SingleMemberAccessor;
import pt.isel.mpd.tf.annotation.ForeignKey;
import pt.isel.mpd.tf.builder.DataMapperBuilder;
import pt.isel.mpd.tf.exception.BuilderException;

public class Builder {

	private MemberAccessorFactory factory;
	private DataMapperBuilder builder;
	private Map<Class<?>, Map<String, MemberAccessor>> klassMap = new HashMap<>();

	public Builder(DataMapperBuilder builder, MemberAccessorFactory factory) {
		this.builder = builder;
		this.factory = factory;
	}

	public <T> DataMapper<T> build(Class<T> klass) {
		try {
			return builder.build(klass, getMemberAccessorMap(klass));
		} catch (Exception e) {
			throw new BuilderException("Error getting member accessors", e);
		}
	}

	private <T> Map<String, MemberAccessor> getMemberAccessorMap(Class<T> klass)
			throws NoSuchMethodException, SecurityException {

		if (klassMap.containsKey(klass)) {
			return klassMap.get(klass);
		}

		Map<String, MemberAccessor> m = factory.getMemberAccessors(klass);
		klassMap.put(klass, m);

		for (Map.Entry<String, MemberAccessor> e : m.entrySet()) {
			MemberAccessor mac = e.getValue();
			ForeignKey fk = mac.getAnnotation(ForeignKey.class);
			if (fk == null)
				continue;

			Class<?> fkClass = mac.getType();
			boolean isIterable = false;

			if (fkClass.equals(Iterable.class)) { // Only accept Iterable
				fkClass = (Class<?>) mac.getGenericType()
						.getActualTypeArguments()[0];
				isIterable = true;
			}

			Map<String, MemberAccessor> m2 = getMemberAccessorMap(fkClass);
			MemberAccessor newMac = isIterable ? new MultipleMemberAccessor(
					fkClass, m2, mac, fk, builder.build(fkClass, m2))
					: new SingleMemberAccessor(fkClass, m2, mac, fk,
							builder.build(fkClass, m2));
			m.put(e.getKey(), newMac);

		}

		return m;
	}
}
