package pt.isel.mpd.tf.accessors;

import java.util.Map;

public interface MemberAccessorFactory {

	/**
	 * Creates @MemberAccessor for the members of the specified class and
	 * returns a map with the name and specified @MemberAccessor Must also set
	 * the primary key if found
	 * 
	 * @param klass
	 *            The class
	 * @return A map of @MemberAccessor
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 */
	Map<String, MemberAccessor> getMemberAccessors(/* in */Class<?> klass)
			throws NoSuchMethodException, SecurityException;
}
