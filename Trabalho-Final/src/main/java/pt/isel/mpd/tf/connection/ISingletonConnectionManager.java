package pt.isel.mpd.tf.connection;

import java.sql.Connection;
import java.sql.SQLException;

public interface ISingletonConnectionManager extends IConnectionManager {

	Connection beginBatch() throws SQLException;

	void endBatch() throws SQLException;

}
