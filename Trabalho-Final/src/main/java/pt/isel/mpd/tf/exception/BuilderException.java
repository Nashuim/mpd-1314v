package pt.isel.mpd.tf.exception;

public class BuilderException extends RuntimeException {

	private static final long serialVersionUID = -9073058935316408110L;

	public BuilderException() {
		super();
	}

	public BuilderException(String msg) {
		super(msg);
	}

	public BuilderException(String msg, Exception e) {
		super(msg, e);
	}
}
