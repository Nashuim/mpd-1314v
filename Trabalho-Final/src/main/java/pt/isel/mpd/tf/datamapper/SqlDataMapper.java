package pt.isel.mpd.tf.datamapper;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

import pt.isel.mpd.tf.SqlIterable;
import pt.isel.mpd.tf.accessors.MemberAccessor;
import pt.isel.mpd.tf.annotation.PrimaryKey;
import pt.isel.mpd.tf.annotation.Table;
import pt.isel.mpd.tf.connection.IConnectionManager;
import pt.isel.mpd.tf.exception.BuilderException;
import pt.isel.mpd.tf.util.SqlStringBuilder;

public class SqlDataMapper<T> extends AbstractDataMapper<T> {

	private IConnectionManager manager;
	private String tableName;
	private String primaryKey;
	private String primaryKeyMemberName;
	private boolean pkIdentity;

	public SqlDataMapper(Class<T> klass, Map<String, MemberAccessor> map,
			IConnectionManager manager) {
		super(klass, map);
		this.manager = manager;

		setTableName();
		setPrimaryKey();
	}

	private void setPrimaryKey() {
		iterateAndExecute((Map.Entry<String, MemberAccessor> e) -> {

			PrimaryKey key = e.getValue().getAnnotation(PrimaryKey.class);
			if (key != null) {
				this.primaryKeyMemberName = e.getKey();
				this.primaryKey = key.name();
				this.pkIdentity = key.identity();
				return;
			}

		});

		if (primaryKeyMemberName == null)
			throw new BuilderException(
					"Class must define a PrimaryKey name using the annotation @PrimaryKey");
	}

	private void setTableName() {
		Table table = getKlass().getAnnotation(Table.class);
		if (table == null)
			throw new BuilderException(
					"Class must define a table name using the annotation @Table");

		String tableName = table.value();

		if (tableName == null || tableName.trim().isEmpty())
			throw new BuilderException("Class must define a table name");

		this.tableName = tableName;
	}

	@Override
	public SqlIterable<T> getAll() {
		return new DataIterable(tableName);
	}

	@Override
	public void update(T val) {
		String pk = getPK();

		String[] values = getNamesWithoutPK(pk);

		String sql = SqlStringBuilder.beginUpdate(tableName).set(values)
				.where(pk + " = " + "?").build();

		Object pkVal = getPrimaryKeyObj(val);

		executeUpdateAndPutValues(val, sql, pk,
				(j, del) -> del.setObject(j.i, pkVal));
	}

	@Override
	public void delete(T val) {

		String pk = getPK();

		String sql = SqlStringBuilder.beginDelete(tableName)
				.where(pk + " = " + "?").build();

		Object pkVal = getPrimaryKeyObj(val);
		executeUpdate(sql, del -> del.setObject(1, pkVal));

	}

	@Override
	public void insert(T val) {
		String pk = getPK();

		String[] values = getNamesWithoutPK(pk);

		String sql = SqlStringBuilder.beginInsert(tableName).values(values)
				.build();

		executeUpdateAndPutValues(val, sql, pk, null);

	}

	private Object getPrimaryKeyObj(T val) {
		MemberAccessor pkAcc = getAccessors().get(primaryKeyMemberName);
		Object pkVal = null;
		try {
			pkVal = pkAcc.get(val);
		} catch (IllegalArgumentException | IllegalAccessException
				| InvocationTargetException e) {
			throw new BuilderException("Error accessing Primary Key", e);
		}

		return pkVal;
	}

	private void executeUpdateAndPutValues(T val, String sql, String pk,
			SqlBiConsumer<Incrementor, PreparedStatement> extra) {
		Incrementor j = new Incrementor(1);
		executeUpdate(
				sql,
				del -> {
					iterateAndExecute(e -> {

						try {
							if (!pkIdentity
									|| !e.getKey().equals(primaryKeyMemberName)) {
								Object obj = e.getValue().get(val);
								del.setObject(j.i, obj);
								j.inc();
							}
						} catch (Exception ex) {
							throw new BuilderException(
									"Error accessing object ", ex);
						}

					});

					if (extra != null)
						extra.accept(j, del);
				});
	}

	private String[] getNamesWithoutPK(String pk) {

		String[] values = new String[getAccessors().size() - 1];

		// String[] values = getAccessors().keySet().toArray(new String[]{});
		Incrementor i = new Incrementor(0);
		iterateAndExecute((Map.Entry<String, MemberAccessor> e) -> {
			if (!pkIdentity || !e.getKey().equals(primaryKeyMemberName)) {
				values[i.i] = e.getKey();
				i.inc();
			}
		});

		return values;
	}

	private void executeUpdate(String sql, SqlConsumer<PreparedStatement> cons) {
		Connection conn = null;
		PreparedStatement del = null;
		try {
			conn = manager.beginStatement();
			del = conn.prepareStatement(sql);

			cons.accept(del);

			del.executeUpdate();
		} catch (SQLException e) {
			throw new BuilderException("Sql error ", e);
		} finally {

			if (conn != null) {
				try {
					manager.endStatement(conn);
				} catch (SQLException e) {
					throw new BuilderException("Error closing connection ", e);
				}
			}

			if (del != null) {
				try {
					del.close();
				} catch (SQLException e) {
					throw new BuilderException("Error closing command ", e);
				}
			}
		}

	}

	private String getPK() {
		String pk = primaryKey;

		if (pk == null || pk.trim().isEmpty()) {
			pk = primaryKeyMemberName;
		}
		return pk;
	}

	@FunctionalInterface
	private interface SqlConsumer<T> {
		void accept(T t) throws SQLException;
	}

	@FunctionalInterface
	private interface SqlBiConsumer<T, U> {
		void accept(T t, U u) throws SQLException;
	}

	private static class Incrementor {
		public int i;

		public Incrementor(int i) {
			this.i = i;
		}

		public void inc() {
			i++;
		}
	}

	private class DataIterable implements SqlIterable<T> {

		private String tableName;
		private String where;
		private PreparedStatement sel;
		private Object[] objs;

		public DataIterable(String tableName) {
			this.tableName = tableName;
		}

		public DataIterable(String tableName, String where, Object[] objs) {
			this.tableName = tableName;
			this.where = where;
			this.objs = objs;
		}

		@Override
		public Iterator<T> iterator() {
			return new Iterator<T>() {
				boolean consumed = true;
				ResultSet rs = null;

				@Override
				public boolean hasNext() {
					try {
						if (rs == null) {
							String sql = conditionalWhere(
									SqlStringBuilder.beginSelect(tableName))
									.build();
							rs = executeQuery(sql);
						}

						if (!consumed)
							return true;

						consumed = false;
						if (!rs.next()) {
							rs.close();
							return false;
						}

						return true;
					} catch (SQLException e) {
						throw new BuilderException("error on cursor ", e);
					}
				}

				@Override
				public T next() {

					if (hasNext()) {
						T t;
						try {
							t = getKlass().newInstance();
						} catch (IllegalAccessException
								| InstantiationException e) {
							throw new BuilderException(
									"error creating a new instance", e);
						}

						iterateAndExecute((
								Map.Entry<String, MemberAccessor> entry) -> {

							MemberAccessor accessor = entry.getValue();
							String name = entry.getKey();
							name = name == primaryKeyMemberName ? getPK()
									: name;

							try {
								Object value = null;
								try {
									value = rs.getObject(name);
								} catch (SQLException e) {
									value = rs.getObject(getPK());
								}
								accessor.set(t, value);
							} catch (Exception e) {
								throw new BuilderException(
										"Error binding the value", e);
							}
						});
						consumed = true;
						return t;
					}
					throw new NoSuchElementException();
				}
			};
		}

		@Override
		public void close() throws Exception {
			if (sel != null)
				sel.close();
		}

		@Override
		public SqlIterable<T> where(String clause) {
			String whr;
			if (where == null)
				whr = clause;
			else {
				whr = SqlStringBuilder.continueFrom(where).and().append(clause)
						.build();
			}
			return new DataIterable(tableName, whr, objs);
		}

		@Override
		public int count() {
			String sql = conditionalWhere(
					SqlStringBuilder.beginSelectCount(tableName)).build();
			ResultSet rs = executeQuery(sql);
			int count = -1;
			try {
				rs.next();
				count = rs.getInt(1);
				rs.close();
			} catch (SQLException e) {
				throw new BuilderException("Error executing count", e);
			}

			return count;
		}

		@Override
		public SqlIterable<T> bind(Object... objs) {
			return new DataIterable(tableName, where, objs);
		}

		private ResultSet executeQuery(String sql) {
			if (sel != null)
				throw new BuilderException(
						"A SqlIterable can only execute one command");
			Connection con = null;
			ResultSet rs = null;
			try {
				con = manager.beginStatement();
				sel = con.prepareStatement(sql);
				bindValues();
				rs = sel.executeQuery();
			} catch (SQLException e) {
				throw new BuilderException("Sql error ", e);
			} finally {
				if (con != null)
					try {
						manager.endStatement(con);
					} catch (SQLException e) {
						throw new BuilderException("Error closing connection ",
								e);
					}
			}
			return rs;
		}

		private void bindValues() throws SQLException {
			if (objs == null)
				return;

			for (int i = 0; i < objs.length; ++i)
				sel.setObject(i + 1, objs[i]);

		}

		private SqlStringBuilder conditionalWhere(SqlStringBuilder builder) {
			if (where == null)
				return builder;
			else
				return builder.where(where);
		}

	}
}
